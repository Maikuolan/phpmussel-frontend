##\
# This file is a part of the phpMussel\FrontEnd package.
# Homepage: https://phpmussel.github.io/
#
# PHPMUSSEL COPYRIGHT 2013 AND BEYOND BY THE PHPMUSSEL TEAM.
#
# License: GNU/GPLv2
# @see LICENSE.txt
#
# This file: Bosnian language data (last modified: 2025.02.08).
#
# Regarding translations: My native language is English. Because this is a free
# and open-source hobby project which generates zero income, and translatable
# content is likely to change as the features and functionality supported by
# the project changes, it doesn't make sense for me to spend money for
# translations. Because I'm the sole author/developer/maintainer for the
# project and I'm not a ployglot, any translations I produce are very likely to
# contain errors. Sorry, but realistically, that won't ever change. If you find
# any such errors/typos/mistakes/etc, your assistance to correct them would be
# very much appreciated. Pull requests are invited and encouraged. Otherwise,
# if you find these errors too much to handle, just stick with the original
# English source. If a translation is irredeemably incomprehensible, let me
# know which, and I can delete it. If you're not sure how to perform pull
# requests, ask. I can help.
##/

checkbox:
 Delete orphaned values: "Izbrišite izgubljene vrijednosti."
config:
 core: "Opća konfiguracija (svaka konfiguracija jezgra koja ne pripada drugim kategorijama)."
 core_default_timeout: "Zadano vremensko ograničenje za korištenje za eksterne zahtjeve? Zadano = 12 sekundi."
 core_delete_on_sight: "Omogućavanje ove direktive će uputiti skriptu da odmah pokuša da izbriše svaki skenirani pokušaj otpremanja datoteka koji odgovara bilo kom kriterijumu detekcije, bilo putem potpisa ili na neki drugi način. Datoteke označeni kao "čisti" se neće dirati. U slučaju arhiva, cijela arhiva će biti izbrisana, bez obzira na to da li je uvredljivi datoteka samo jedan od nekoliko datoteka koji se nalaze u arhivi. U slučaju skeniranja učitavanja datoteka, obično nije potrebno omogućiti ovu direktivu, jer obično PHP automatski čisti sadržaj svoje keš memorije kada se izvršavanje završi, što znači da će obično izbrisati sve datoteke koje su postavljene preko njega na server osim ako već nisu premještene, kopirane ili izbrisane. Ova direktiva je ovde dodata kao dodatna mera bezbednosti za one čije se kopije PHP-a možda ne ponašaju uvek na očekivani način. False/Lažno = Nakon skeniranja, ostavite datoteku na miru [Zadano]; True/Istinito = Nakon skeniranja, ako nije čisto, odmah izbrišite."
 core_disabled_channels: "Ovo se može koristiti da spriječi phpMussel da koristi određene kanale prilikom slanja zahtjeva."
 core_error_log: "Datoteka za evidentiranje svih otkrivenih grešaka koje nisu fatalne. Navedite naziv datoteke ili ostavite prazno da biste onemogućili."
 core_hide_version: "Sakriti informacije o verziji iz dnevnika i izlaza stranice? True/Istinito = Da; False/Lažno = Ne [Zadano]."
 core_ipaddr: "Gdje pronaći IP adresu zahtjeva za povezivanje? (Korisno za usluge kao što je Cloudflare i slično). Zadano = REMOTE_ADDR. UPOZORENJE: Promijenite ovo samo ako znate šta radite!"
 core_lang: "Odredite podrazumevani jezik za phpMussel."
 core_lang_override: "Lokalizirati prema HTTP_ACCEPT_LANGUAGE kad god je to moguće? True/Istinito = Da [Zadano]; False/Lažno = Ne."
 core_log_rotation_action: "Rotacija dnevnika ograničava broj datoteka dnevnika koji bi trebali postojati u bilo kojem trenutku. Kada se kreiraju nove datoteke evidencije, ako ukupan broj datoteka dnevnika premašuje navedeno ograničenje, navedena radnja će se izvršiti. Ovdje možete odrediti željenu akciju."
 core_log_rotation_limit: "Rotacija dnevnika ograničava broj datoteka dnevnika koji bi trebali postojati u bilo kojem trenutku. Kada se kreiraju nove datoteke evidencije, ako ukupan broj datoteka dnevnika premašuje navedeno ograničenje, navedena radnja će se izvršiti. Ovdje možete odrediti željeni limit. Vrijednost 0 će onemogućiti rotaciju dnevnika."
 core_maintenance_mode: "Omogućiti način održavanja? True/Istinito = Da; False/Lažno = Ne [Zadano]. Onemogućuje sve osim front-enda. Ponekad korisno za ažuriranje vašeg CMS-a, okvira, itd."
 core_outbound_request_log: "Datoteka za evidentiranje rezultata svih odlaznih zahtjeva. Navedite naziv datoteke ili ostavite prazno da biste onemogućili."
 core_scan_cache_expiry: "Koliko dugo phpMussel treba da kešira rezultate skeniranja? Vrijednost je broj sekundi za keširanje rezultata skeniranja. Podrazumevano je 21600 sekundi (6 sati); Vrijednost 0 će onemogućiti keširanje rezultata skeniranja."
 core_scan_log: "Ime datoteke u koju se evidentiraju svi rezultati skeniranja. Navedite naziv datoteke ili ostavite prazno da biste onemogućili."
 core_scan_log_serialized: "Ime datoteke u koju se evidentiraju svi rezultati skeniranja (koristeći serijalizirani format). Navedite naziv datoteke ili ostavite prazno da biste onemogućili."
 core_statistics: "Pratite statistiku upotrebe phpMussel? True/Istinito = Da; False/Lažno = Ne [Zadano]."
 core_time_format: "Format notacije datuma/vremena koji koristi phpMussel. Dodatne opcije se mogu dodati na zahtjev."
 core_time_offset: "Pomak vremenske zone u minutama."
 core_timezone: "Vremenska zona koju treba koristiti (npr., Africa/Cairo, America/New_York, Asia/Tokyo, Australia/Perth, Europe/Berlin, Pacific/Guam, etc). Odredite "SYSTEM" da dozvolite PHP-u da to automatski obavi umjesto vas."
 core_truncate: "Skratiti datoteke dnevnika kada dostignu određenu veličinu? Vrijednost je maksimalna veličina u B/KB/MB/GB/TB do koje datoteka evidencije može narasti prije nego što bude skraćena. Zadana vrijednost od 0KB onemogućuje skraćivanje (datoteke evidencije mogu beskonačno rasti). Bilješka: Odnosi se na pojedinačne log datoteke! Veličina log datoteka se ne razmatra zajedno."
 experimental: "Nestabilno/Eksperimentalno!"
 files: "Kako rukovati datotekama prilikom skeniranja."
 files_allow_leading_trailing_dots: "Dozvoliti vodeće i zadnje tačke u nazivima datoteka? Ovo se ponekad može koristiti za sakrivanje datoteka ili za prevariti neke sisteme da dozvole prelazak preko direktorija. False/Lažno = Ne dozvoli [Zadano]. True/Istinito = Dozvoli."
 files_archive_file_extensions: "Prepoznate ekstenzije arhivskih datoteka (format je CSV; treba dodati ili ukloniti samo kada se pojave problemi; nepotrebno uklanjanje može uzrokovati pojavu lažnih rezultata za arhivske datoteke, dok će nepotrebni dodaci u suštini staviti na bijelu listu ono što dodajete iz detekcije specifičnog za napad; modificirajte s oprezom; također imajte na umu da ovo nema utjecaja na to koje arhive mogu, a koje ne mogu biti analizirane na nivou sadržaja). Lista, kao što je podrazumevano, navodi one formate koji se najčešće koriste u većini sistema i CMS-a, ali namerno nije nužno sveobuhvatna."
 frontend: "Konfiguracija za front-end."
 frontend_custom_footer: "Umetnuto kao HTML na samom dnu svih front-end stranica. Ovo bi moglo biti korisno u slučaju da na svim takvim stranicama želite uključiti pravnu obavijest, vezu za kontakt, poslovne informacije, ili slično."
 frontend_custom_header: "Umetnuto kao HTML na samom početku svih front-end stranica. Ovo bi moglo biti korisno u slučaju da na svim takvim stranicama želite uključiti logo web stranice, personalizirano zaglavlje, skripte, ili slično."
 frontend_default_algo: "Definira koji algoritam koristiti za sve buduće lozinke i sesije."
 frontend_frontend_log: "Datoteka za evidentiranje pokušaja front-end prijave. Navedite naziv datoteke ili ostavite prazno da biste onemogućili."
 frontend_magnification: "Uvećanje fonta. Zadano = 1."
 frontend_max_login_attempts: "Maksimalan broj front-end pokušaja prijave. Zadano = 5."
 frontend_numbers: "Kako želite da se brojevi prikazuju? Odaberite primjer koji vam se čini najispravnijim."
 legal: "Konfiguracija za zakonske zahtjeve."
 legal_privacy_policy: "Adresa relevantne politike privatnosti koja će se prikazati u podnožju svake generisane stranice. Navedite URL, ili ostavite prazno da biste onemogućili."
 legal_pseudonymise_ip_addresses: "Pseudonimizirati IP adrese prilikom prijavljivanja? True/Istinito = Da [Zadano]; False/Lažno = Ne."
 supplementary_cache_options: "Dodatne opcije keša. Bilješka: Promjena ovih vrijednosti može vas potencijalno odjaviti."
 supplementary_cache_options_enable_apcu: "Određuje hoće li pokušati koristiti APCu za keširanje. Zadano = True."
 supplementary_cache_options_enable_memcached: "Određuje hoće li se pokušati koristiti Memcached za keširanje. Zadano = False."
 supplementary_cache_options_enable_pdo: "Određuje hoće li pokušati koristiti PDO za keširanje. Zadano = False."
 supplementary_cache_options_enable_redis: "Određuje hoće li pokušati koristiti Redis za keširanje. Zadano = False."
 supplementary_cache_options_memcached_host: "Memcached vrijednost hosta. Zadano = "localhost"."
 supplementary_cache_options_memcached_port: "Memcached vrijednost porta. Zadano = "11211"."
 supplementary_cache_options_pdo_dsn: "Memcached vrijednost DSN-a. Zadano = "mysql:dbname=phpmussel;host=localhost;port=3306"."
 supplementary_cache_options_pdo_password: "PDO lozinka."
 supplementary_cache_options_pdo_username: "PDO korisničko ime."
 supplementary_cache_options_prefix: "Ovdje navedena vrijednost će biti dodata svim ključevima za unos u keš memoriju. Zadano = "phpMussel_". Kada postoji više instalacija na istom serveru, ovo može biti korisno za držanje njihovih kešova odvojenih jedan od drugog."
 supplementary_cache_options_redis_database_number: "Redis broj baze podataka. Zadano = 0. Bilješka: Ne mogu koristiti druge vrijednosti osim 0 s Redis klasterom."
 supplementary_cache_options_redis_host: "Redis vrijednost hosta. Zadano = "localhost"."
 supplementary_cache_options_redis_port: "Redis vrijednost porta. Zadano = "6379"."
 supplementary_cache_options_redis_timeout: "Redis vrijednost vremenskog ograničenja. Zadano = "2.5"."
confirm:
 Action: "Jeste li sigurni da želite "%s"?"
field:
 2FA code: "2FA kod"
 Clear all: "Obriši sve"
 Confirm: "Potvrdi"
 Create new account: "Kreirajte novi račun"
 Delete account: "Izbrišite račun"
 Delete all: "Izbriši sve"
 Delete: "Izbriši"
 Download: "Preuzmi"
 False (False): "False (Lažno)"
 File: "Datoteka"
 Latest version: "Najnovija verzija"
 Log In: "Prijavite se"
 More fields: "Više polja"
 OK: "OK"
 Options: "Opcije"
 Password: "Lozinka"
 Permissions: "Dozvole"
 Quarantine key: "Ključ za karantin"
 Reset: "Resetujte"
 Restore: "Vrati"
 Set new password: "Postavite novu lozinku"
 True (True): "True (Istinito)"
 Update signature files: "Ažurirajte datoteke potpisa"
 Update: "Ažurirajte"
 Use system default timezone: "Koristite zadanu vremensku zonu sistema."
 Username: "Korisničko ime"
 Your version: "Vaša verzija"
 log_rotation_archive: "Prvo arhivirajte, a zatim izbrišite najstarije datoteke dnevnika, tako da ograničenja više neće biti prekoračena."
 log_rotation_delete: "Izbrišite najstarije datoteke dnevnika, tako da ograničenja više neće biti prekoračena."
 size:
  Total size: "Ukupna veličina:"
  bytes:
   - "bajt"
   - "bajta"
   - "bajtova"
hints_logging_datetime: "Koristan savjet: Možete priložiti informacije o datumu/vremenu imenima datoteka evidencije koristeći čuvare mjesta u formatu vremena. Dostupni čuvari mjesta formata vremena prikazani su na {{Links.ConfigRef.time_format}}."
hints_pdo_dsn:
 Čpp.: "<em><a href="https://github.com/phpMussel/Docs/blob/master/readme.en.md\#user-content-HOW_TO_USE_PDO" hreflang="en-AU">Šta je "PDO DSN"? Kako mogu koristiti PDO sa phpMussel-om?</a></em>"
response:
 _No: "Ne"
 _Yes: "Da"
siginfo_key_Testfile: "Potpisi za probne datoteke (tj., ne otkrivanja zlonamjernih)."
siginfo_key_Total: "Ukupno aktivnih potpisa."
siginfo_key_VT: "Potpisi uključujući ili zasnovane na podacima iz Virus Total."
siginfo_key_Werewolf: "Potpisi koji se bave "napadima vukodlaka"."
siginfo_sub_Classes: "Brojanje prema klasi datoteke potpisa"
siginfo_sub_Files: "Brojanje prema datoteku potpisa"
siginfo_sub_MalwareTypes: "Brojanje prema vrsti infekcije ili zlonamjernog softvera"
siginfo_sub_SigTypes: "Brojanje prema metapodacima potpisa"
siginfo_sub_Targets: "Brojanje prema ciljanom vektoru"
siginfo_sub_Vendors: "Brojanje prema dobavljaču potpisa ili izvoru"
siginfo_xkey: "Identificiran kao "%s"."
state_quarantine:
 - "Trenutno je %s datoteka u karantinu."
 - "Trenutno su %s datoteke u karantinu."
 - "Trenutno se %s datoteka u karantinu."
tip:
 2FA Sent: "Poruka e-pošte koja sadrži dvofaktorni kod za autentifikaciju poslana je na vašu adresu e-pošte. Potvrdite ovaj kod u nastavku da dobijete pristup front-endu. Ako niste primili ovu e-poštu, pokušajte se odjaviti, pričekajte 10 minuta, i ponovo se prijavite da biste primili novu e-poštu koja sadrži novi kod."
 Accounts: "Stranica računa vam omogućava da kontrolišete ko može da pristupi phpMussel front-endu."
 Cache Data: "Ovdje možete pregledati sadržaj keša."
 Configuration: "Stranica za konfiguraciju vam omogućava da modificirate konfiguraciju za phpMussel sa front-enda."
 Greeting: "Zdravo, %s."
 Home: "Ovo je početna stranica za phpMussel front-end. Odaberite vezu iz navigacijskog menija na lijevoj strani da nastavite."
 Logs: "Izaberite datoteku evidencije sa liste ispod da vidite sadržaj te datoteke evidencije."
 Quarantine is currently disabled: "Bilješka: Karantena je trenutno onemogućena, ali se može omogućiti putem konfiguracijske stranice."
 Quarantine: "Ova stranica navodi sve datoteke koje su trenutno u karantinu i olakšava upravljanje tim datotekama."
 See the documentation: "Pogledajte <a href="https://github.com/phpMussel/Docs/blob/master/readme.en.md\#user-content-SECTION5" hreflang="en" rel="noopener external">dokumentaciju</a> za informacije o različitim konfiguracijskim direktivama i njihovoj svrsi."
 Signature Information: "Stranica sa informacijama o potpisu pruža neke osnovne informacije o izvorima i tipovima trenutno aktivnih potpisa."
 Statistics tracking is currently disabled: "Bilješka: Praćenje statistike je trenutno onemogućeno, ali se može omogućiti putem konfiguracijske stranice."
 Statistics: "Ova stranica prikazuje neke osnovne statistike upotrebe u vezi sa vašom instalacijom phpMussel-a."
 Upload Testing: "Stranica za testiranje učitavanja sadrži standardni obrazac za otpremanje datoteka, koji vam omogućava da testirate da li bi phpMussel normalno blokirao datoteku kada pokušavate da ga otpremite."
warning:
 Cookies: "Bilješka: phpMussel koristi kolačić za provjeru autentičnosti prijava. Prijavom dajete pristanak da vaš pretraživač kreira i pohrani kolačić."
 JavaScript required: "Upozorenje: Neke front-end funkcionalnosti zahtijevaju JavaScript. Kako biste osigurali da se cijeli front-end ponaša kako se očekuje, preporučuje se omogućavanje JavaScripta."
 Maintenance mode is enabled: "Upozorenje: Režim održavanja je omogućen!"
 No signature files are active: "Upozorenje: Nijedan datoteka potpisa nije omogućen!"
 Proceeding will log out all users: "Upozorenje: Nastavkom će se odjaviti svi korisnici."
 This account is not using a valid password: "Upozorenje: Ovaj račun ne koristi važeću lozinku!"
 Using the default password: "Upozorenje: Korištenje zadane lozinke!"
